const data = require('./question2.js');

const fullNameObject = data.filter((object) => {
    let firstName = object['first_name'];
    let lastName = object['last_name'];
    let fullName = firstName + " " + lastName;

    object['full_name'] = fullName;

    return object;
});

console.log(fullNameObject);