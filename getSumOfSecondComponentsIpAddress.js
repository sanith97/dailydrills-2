const data = require('./question2.js');

const sumOfSecondComponents = data.map((object) => {
    let ipAddress = object['ip_address'];
    let componentIpAddress = ipAddress.split('.');

    return Number(componentIpAddress[1]);

}).reduce((previousValue, currentValue) => {

    return (previousValue + currentValue);
})

console.log(sumOfSecondComponents);