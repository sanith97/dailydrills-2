const data = require('./question2.js');

data.sort((object1, object2) => {
    let name1 = object1['first_name'];
    let name2 = object2['first_name'];

    if(name1 < name2) {
        return 1;
    }

    if(name1 > name2) {
        return -1;
    }

    return 0;

});

console.log(data);


