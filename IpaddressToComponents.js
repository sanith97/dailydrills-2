const data = require('./question2.js');

const IpAddressToComponents = data.map((object) => {
    let ipAddress = object['ip_address'].split('.');
    
    object['ip_address'] = ipAddress;

    return object;
});

console.log(IpAddressToComponents);