const data = require('./question2.js');

const requiredDomains = ['org', 'au', 'com'];

const requiredDomainObjects = data.filter((object) => {
    let splittedEmail = object['email'].split('.');
    let lastIndex = (splittedEmail.length) - 1;
    //picking last index to get domain details
    let objectDomain = splittedEmail[lastIndex];

    let isDomainPresent = requiredDomains.includes(objectDomain) ? true : false;

    return isDomainPresent;
})

console.log(requiredDomainObjects.length);