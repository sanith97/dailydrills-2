const data = require('./question2.js');

const sumOfFourthComponents = data.map((object) => {
    let ipAddress = object['ip_address'];
    let componentIpAddress = ipAddress.split('.');

    return Number(componentIpAddress[3]);

}).reduce((previousValue, currentValue) => {

    return (previousValue + currentValue);
})

console.log(sumOfFourthComponents);